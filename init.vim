" vim: filetype=vim foldmethod=marker foldlevel=0 foldcolumn=3

"" PREAMBLE {{{1
let g:python3_host_prog = expand('/home/robin/.pyenv/versions/neovim-3.7.4/bin/python')
let g:python_host_prog = expand('/home/robin/.pyenv/versions/neovim-2.7.11/bin/python')

" let g:python_host_prog = expand('~/.virtualenvs/neovim3/bin/python')

set langmenu=en_US
let $LANG = 'en_US'
source $VIMRUNTIME/delmenu.vim
source $VIMRUNTIME/menu.vim

""}}}

"" PLUGINS {{{1
" auto-install vim-plug
if empty(glob('~/.config/nvim/autoload/plug.vim'))
  silent !curl -fLo ~/.config/nvim/autoload/plug.vim --create-dirs \
      https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
  autocmd VimEnter * PlugInstall
endif
call plug#begin(expand('~/.config/nvim/plugged'))

" CORE

Plug 'tpope/vim-dispatch'
Plug 'radenling/vim-dispatch-neovim'

" NAVIGATION

Plug 'scrooloose/nerdtree'
Plug 'Xuyuanp/nerdtree-git-plugin'
Plug 'jistr/vim-nerdtree-tabs'
Plug 'vim-scripts/grep.vim'
Plug 'ctrlpvim/ctrlp.vim'
Plug 'yangmillstheory/vim-snipe'
Plug 'justinmk/vim-sneak'
Plug 'mileszs/ack.vim'

if isdirectory('/usr/local/opt/fzf')
  Plug '/usr/local/opt/fzf' | Plug 'junegunn/fzf.vim'
else
  Plug 'junegunn/fzf', { 'dir': '~/.fzf', 'do': './install --bin' }
  Plug 'junegunn/fzf.vim'
endif

" EDITING

Plug 'bronson/vim-trailing-whitespace'
Plug 'jiangmiao/auto-pairs'
Plug 'tpope/vim-commentary'
Plug 'tpope/vim-repeat'
Plug 'tpope/vim-surround'
Plug 'tpope/vim-abolish'
Plug 'michaeljsmith/vim-indent-object'
Plug 'PeterRincker/vim-argumentative'
Plug 'alvan/vim-closetag'
Plug 'junegunn/vim-easy-align'
Plug 'godlygeek/tabular'

" UI/UX

Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'
Plug 'airblade/vim-gitgutter'
Plug 'vim-scripts/CSApprox'
Plug 'nathanaelkane/vim-indent-guides'
Plug 'junegunn/vim-peekaboo'
Plug 'lifepillar/vim-solarized8'

" RUNNING TESTS

Plug 'janko-m/vim-test'

" LINTING

Plug 'w0rp/ale'

" SCM

Plug 'tpope/vim-fugitive'
Plug 'k0kubun/vim-open-github'


" AUTOCOMPLETION

" if has('nvim')
"   Plug 'Shougo/deoplete.nvim', { 'do': ':UpdateRemotePlugins' }
" else
"   Plug 'Shougo/deoplete.nvim'
"   Plug 'roxma/nvim-yarp'
"   Plug 'roxma/vim-hug-neovim-rpc'
" endif
" let g:deoplete#enable_at_startup = 1

" Plug 'zchee/deoplete-jedi'
Plug 'neoclide/coc.nvim', {'branch': 'release'}

" LANGUAGES

Plug 'sheerun/vim-polyglot'

" Javascript et al.
Plug 'jelera/vim-javascript-syntax'
Plug 'mxw/vim-jsx'
" Plug 'carlitux/deoplete-ternjs', { 'do': 'npm install -g tern' }
Plug 'prettier/vim-prettier', { 'do': 'yarn install' }

" Python
Plug 'davidhalter/jedi-vim'
Plug 'tmhedberg/SimpylFold'
Plug 'jmcantrell/vim-virtualenv'
Plug 'jeetsukumaran/vim-pythonsense'
Plug 'ambv/black'

" Django
" Plug 'vim-scripts/django.vim'
Plug 'tweekmonster/django-plus.vim'


" Sass/scss/haml
Plug 'tpope/vim-haml'

" Jinja

Plug 'lepture/vim-jinja'


" Rust
"
Plug 'rust-lang/rust.vim'
Plug 'racer-rust/vim-racer'

Plug 'jparise/vim-graphql'

call plug#end()

"" }}}

"" BASIC SETUP {{{1

"" Encoding
set encoding=utf-8
set fileencoding=utf-8
set fileencodings=utf-8
set bomb
set binary

" read in external changes automatically
set autoread
au FocusGained,BufEnter * :silent! !
"
" save on focus lost
au FocusLost * silent! wa

" don't redraw during macro execution
set lazyredraw

"" Fix backspace indent
set backspace=indent,eol,start

"" highlight line with cursor
set cursorline

filetype plugin indent on

" configure expanding of tabs for various file types
au BufRead,BufNewFile *.py set expandtab

set expandtab           " enter spaces when tab is pressed
set textwidth=120       " break lines when line length increases
set tabstop=4           " use 4 spaces to represent tab
set softtabstop=4
set shiftwidth=4        " number of spaces to use for auto indent
set autoindent          " copy indent from current line when starting a new line

"" Enable hidden buffers
set hidden

"" Searching
set hlsearch
" but stop highlighting once we press enter or escape
nnoremap <CR> :noh<CR><CR>
nnoremap <esc> :noh<return><esc>
nnoremap <esc>^[ <esc>^[
set incsearch
set ignorecase
set smartcase


" Disable Arrow keys in Escape mode
map <up> <nop>
map <down> <nop>
map <left> <nop>
map <right> <nop>

" Disable Arrow keys in Insert mode
imap <up> <nop>
imap <down> <nop>
imap <left> <nop>
imap <right> <nop>
"" Directories for swp files
set nobackup
set noswapfile

set fileformats=unix,dos,mac

" show commands I am typing in the lower right corner
set showcmd

if !exists('*s:setupWrapping')
  function s:setupWrapping()
    set wrap
    set wm=2
    set textwidth=79
  endfunction
endif

"" txt
augroup vimrc-wrapping
  autocmd!
  autocmd BufRead,BufNewFile *.txt call s:setupWrapping()
augroup END

augroup vimrc-sync-fromstart
  autocmd!
  autocmd BufEnter * :syntax sync maxlines=200
augroup END

"" Remember cursor position
augroup vimrc-remember-cursor-position
  autocmd!
  autocmd BufReadPost * if line("'\"") > 1 && line("'\"") <= line("$") | exe "normal! g`\"" | endif
augroup END


"" }}}

"" VISUAL SETTINGS {{{1
"*****************************************************************************
"" Visual Settings
"*****************************************************************************
syntax on
set ruler
set number

let no_buffers_menu=1
if !exists('g:not_finish_vimplug')
  set background=dark
  let g:solarized_termcolors=256
  colorscheme solarized8
endif

set mousemodel=popup
set t_Co=256
set guioptions=egmrti
set gfn=Monospace\ 10

if has("gui_running")
  if has("gui_mac") || has("gui_macvim")
    set guifont=Menlo:h12
    set transparency=7
  endif
else
  let g:CSApprox_loaded = 1

  " IndentLine
  let g:indentLine_enabled = 1
  let g:indentLine_concealcursor = 0
  let g:indentLine_char = '┆'
  let g:indentLine_faster = 1
endif



"" Disable the blinking cursor.
set gcr=a:blinkon0
set scrolloff=3

"" Status bar
set laststatus=2

"" Use modeline overrides
set modeline
set modelines=10

set title
set titleold="Terminal"
set titlestring=%F

set statusline=%F%m%r%h%w%=(%{&ff}/%Y)\ (line\ %l\/%L,\ col\ %c)\

" Search mappings: These will make it so that going to the next one in a
" search will center on the line it's found in.
nnoremap n nzzzv
nnoremap N Nzzzv

""}}}

"" ABBREVIATIONS AND MAPPINGS {{{1

"" Map leader to ,
let mapleader=','

"" no one is really happy until you have this shortcuts
cnoreabbrev W! w!
cnoreabbrev Q! q!
cnoreabbrev Qall! qall!
cnoreabbrev Wq wq
cnoreabbrev Wa wa
cnoreabbrev wQ wq
cnoreabbrev WQ wq
cnoreabbrev W w
cnoreabbrev Q q
cnoreabbrev Qall qall

"" Close buffer
noremap <leader>c :bp<CR>:bd #<CR>

"" Vmap for maintain Visual Mode after shifting > and <
vmap < <gv
vmap > >gv

tnoremap <C-v> <C-\><C-n>

"  simulate |i_CTRL-R| in terminal-mode:
tnoremap <expr> <C-R> '<C-\><C-N>"'.nr2char(getchar()).'pi'

" resize current window
nnoremap <leader>wL :resize +30<CR>
nnoremap <leader>wS :resize -30<CR>
nnoremap <leader>wl :resize +10<CR>
nnoremap <leader>ws :resize -10<CR>


" strip whitespace for the following filetypes
autocmd FileType python,javascript,html,htmldjango,rst,markdown,xsd,xml,rust autocmd BufWritePre <buffer> %s/\s\+$//e

autocmd FileType python abbrev pp from pprint import pprint
autocmd FileType python abbrev ice from icecream import ic<CR>ic(


nmap <leader>ms :Dispatch python manage.py shell_plus<CR>
nmap <leader>pb Oimport ptpdb; ptpdb.set_trace()<ESC>

if !empty($USE_PEP553) && $USE_PEP553
    nmap <leader>b Obreakpoint()<ESC>
else
    nmap <leader>b Oimport ipdb; ipdb.set_trace()<ESC>
endif

nmap <leader>e [mOimport birdseye<CR>@birdseye.eye

"" }}}

"" EASY-ALIGN {{{1

" Start interactive EasyAlign in visual mode (e.g. vipga)
xmap ga <Plug>(EasyAlign)

"" }}}

"" NERDTREE {{{1
let g:NERDTreeChDirMode=2
let g:NERDTreeIgnore=['\.rbc$', '\~$', '\.pyc$', '\.db$', '\.sqlite$', '__pycache__']
let g:NERDTreeSortOrder=['^__\.py$', '\/$', '*', '\.swp$', '\.bak$', '\~$']
let g:NERDTreeShowBookmarks=1
let g:nerdtree_tabs_focus_on_files=1
let g:NERDTreeMapOpenInTabSilent = '<RightMouse>'
let g:NERDTreeWinSize = 30
set wildignore+=*/tmp/*,*.so,*.swp,*.zip,*.pyc,*.db,*.sqlite
nnoremap <silent> <F2> :NERDTreeFind<CR>
noremap <F3> :NERDTreeToggle<CR>

"" }}}

"" FZF.VIM {{{1

set wildmode=list:longest,list:full
set wildignore+=*.o,*.obj,.git,*.rbc,*.pyc,__pycache__
let $FZF_DEFAULT_COMMAND =  "find * -path '*/\.*' -prune -o -path 'node_modules/**' -prune -o -path 'target/**' -prune -o -path 'dist/**' -prune -o  -type f -print -o -type l -print 2> /dev/null"

nnoremap <silent> <C-k> :Tags<CR>

"" }}}

"" DEOPLETE {{{

inoremap <expr> <C-j> pumvisible() ? "\<C-N>" : "\<C-j>"
inoremap <expr> <C-k> pumvisible() ? "\<C-P>" : "\<C-k>"

" }}}

"" PYTHON {{{

au BufRead,BufNewFile *.py setlocal textwidth=100
au BufRead,BufNewFile *.py setlocal formatoptions-=t
let g:black_linelength = 80

"" }}}

"" JEDI {{{

" " don't use the standard jedi completion, use deoplete-jedi because async is the new black.
" let g:jedi#auto_vim_configuration = 0
" let g:jedi#goto_assignments_command = ''  " dynamically done for ft=python.
" let g:jedi#goto_definitions_command = ''  " dynamically done for ft=python.
" let g:jedi#use_tabs_not_buffers = 0  " current default is 1.
" let g:jedi#rename_command = '<Leader>gR'
" let g:jedi#usages_command = '<Leader>gu'
" let g:jedi#completions_enabled = 0
" let g:jedi#smart_auto_mappings = 1

" " Unite/ref and pydoc are more useful.
" let g:jedi#documentation_command = '<Leader>_K'
" let g:jedi#auto_close_doc = 1

" " don't insert import keyword, ffs
" let g:jedi#smart_auto_mappings = 0

autocmd FileType python noremap gd :call jedi#goto_assignments()<CR>

" }}}

"" Jinja {{{

au BufNewfile,BufRead *.tmpl set ft=jinja

" }}}

"" Rust {{{

let g:rustfmt_autosave = 1

"" }}}

"" Javascript {{{

autocmd FileType javascript setlocal shiftwidth=4 tabstop=4 softtabstop=0 expandtab

" }}}

"" ALE {{{

let g:ale_fixers = {
\    'javascript' : [
\        'prettier', 'eslint'
\    ],
\    'python': [
\        'isort',
\        'autopep8',
\        'black',
\    ],
\    'scss': ['stylelint'],
\}

" let g:ale_linters = {
" \   'javascript': ['eslint'],
" \   'python': ['flake8', 'pylint', 'pycodestyle'],
" \}

let g:ale_python_pylint_options = '--load-plugins pylint_django' .  ' --rcfile ' . expand('~/.config/pylint.rc')
let g:ale_python_black_options = '-l ' . g:black_linelength

function! ALERunISort() abort
    try
        let b:ale_fixers={'python': ['isort',]}
        ALEFix
    finally
        unlet b:ale_fixers
    endtry
endfunction

function! ALERunBlack() abort
    try
        let b:ale_fixers={'python': ['black',]}
        ALEFix
    finally
        unlet b:ale_fixers
    endtry
endfunction

nmap ]e :ALENextWrap<CR>
nmap [e :ALEPreviousWrap<CR>

nmap <leader>as :call ALERunISort()<CR>
nmap <leader>ab :call ALERunBlack()<CR>
nmap <leader>ai :!isort -sl % && autoflake --remove-all-unused-imports --in-place % & isort %<CR>

" error format
let g:ale_echo_msg_error_str = 'E'
let g:ale_echo_msg_warning_str = 'W'
let g:ale_echo_msg_format = '[%linter%] %s [%severity%]'

let g:ale_lint_on_text_changed = 'never'

if !empty($VIM_ALE_FIX_ON_SAVE) && $VIM_ALE_FIX_ON_SAVE
    let g:ale_fix_on_save = 1
endif


"}}}

"" Prettier {{{1

let g:prettier#config#print_width = 100
let g:prettier#config#tab_width = 4

"}}}

"" CtrlP {{{1

let g:ctrlp_map = '<c-p>'
let g:ctrlp_working_path_mode = 'r'
let g:ctrlP_custom_ignore = 'node_modules\|DS_Store\|git\|staticfiles\|media_files'

" Use the silver searcher if present
if executable('ag')
  let g:ctrlp_user_command = 'ag %s -l --nocolor --hidden -g ""'
endif

let g:ctrlp_cmd = 'CtrlPMixed'

" make ctrlP play nice with NERDTree
let g:ctrlp_dont_split = 'NERD'

noremap <C-b> :CtrlPBuffer<CR>

"" }}}

"" VIM-TEST {{{1
nmap <silent> <leader>t :w<CR>:TestNearest<CR>
nmap <silent> <leader>T :w<CR>:TestFile<CR>
nmap <silent> <leader>a :w<CR>:TestSuite<CR>
nmap <silent> <leader>l :w<CR>:TestLast<CR>
nmap <silent> <leader>g :w<CR>:TestVisit<CR>

let test#strategy = "dispatch"

let test#python#djangotest#options  =  '--noinput --keepdb'

" Teach vim-test about maykin's project structure
function! DjangoSrcMadnessTransform(cmd) abort
    if filereadable(glob("manage.py"))
        let l:management_file = "manage.py"
        return substitute(substitute(a:cmd, " src.", " ", ""), "manage.py", l:management_file, "")
    elseif filereadable(glob("src/manage.py"))
        let l:management_file = "src/manage.py"
        return substitute(substitute(a:cmd, " src.", " ", ""), "manage.py", l:management_file, "")
    else
        return a:cmd
    endif

endfunction

let g:test#custom_transformations = {'thing': function('DjangoSrcMadnessTransform')}
let g:test#transformation = 'thing'

let test#python#runner = 'djangotest'

"" }}}

"" ACK {{{1

if executable('ag')
  let g:ackprg = 'ag --vimgrep'
endif

" }}}

"" AG {{{1
noremap <C-_> :Ag<CR>

" }}}

"" VIMSCRIPT {{{
autocmd FileType vim setlocal foldmethod=marker
autocmd FileType vim setlocal foldlevel=1

"" }}}

"" RST {{{

au BufRead,BufNewFile *.rst setlocal textwidth=80

"" }}}

"" VIM-AIRLINE {{{
let g:airline_theme = 'powerlineish'
let g:airline#extensions#syntastic#enabled = 0
let g:airline#extensions#branch#enabled = 0
let g:airline#extensions#tabline#enabled = 0
let g:airline#extensions#tagbar#enabled = 1
let g:airline_skip_empty_sections = 1

if !exists('g:airline_symbols')
  let g:airline_symbols = {}
endif

call airline#parts#define_function('ALE', 'ALEGetStatusLine')
call airline#parts#define_condition('ALE', 'exists("*ALEGetStatusLine")')

let g:airline_section_error = airline#section#create_right(['ALE'])
let g:airline_section_error = airline#section#create_right(['ALE'])


if !exists('g:airline_powerline_fonts')
  let g:airline#extensions#tabline#left_sep = ' '
  let g:airline#extensions#tabline#left_alt_sep = '|'
  let g:airline_left_sep          = '▶'
  let g:airline_left_alt_sep      = '»'
  let g:airline_right_sep         = '◀'
  let g:airline_right_alt_sep     = '«'
  let g:airline#extensions#branch#prefix     = '⤴' "➔, ➥, ⎇
  let g:airline#extensions#readonly#symbol   = '⊘'
  let g:airline#extensions#linecolumn#prefix = '¶'
  let g:airline#extensions#paste#symbol      = 'ρ'
  let g:airline_symbols.linenr    = '␊'
  let g:airline_symbols.branch    = '⎇'
  let g:airline_symbols.paste     = 'ρ'
  let g:airline_symbols.paste     = 'Þ'
  let g:airline_symbols.paste     = '∥'
  let g:airline_symbols.whitespace = 'Ξ'
else
  let g:airline#extensions#tabline#left_sep = ''
  let g:airline#extensions#tabline#left_alt_sep = ''

  " powerline symbols
  let g:airline_left_sep = ''
  let g:airline_left_alt_sep = ''
  let g:airline_right_sep = ''
  let g:airline_right_alt_sep = ''
  let g:airline_symbols.branch = ''
  let g:airline_symbols.readonly = ''
  let g:airline_symbols.linenr = ''
endif

""}}}

"" VIM-PEEKABOO {{{
let g:peekaboo_delay = 500
" }}}


" }}}

"" VIM-SNEAK {{{
let g:sneak#label = 1
" }}}

"" CLOSETAG {{{
let g:closetag_filenames = '*.html,*.xhtml,*.xml,*.xsd'
" }}}
